#ifndef MODELS_H
#define MODELS_H


//-----------------------------------------------------------------------
//   public functions
//-----------------------------------------------------------------------


void  declareModels();
void  declareDirichletModel();
void  declareElasticModel();
// void  declareLoadDispModel();
void  declareNeumannModel();
// void  declareSolidModel();
void  declareSpringModel();

#endif


