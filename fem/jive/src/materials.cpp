
#include "materials.h"

#include "LinearIsotropicMaterial.h"

//-----------------------------------------------------------------------
//   declareMaterials
//-----------------------------------------------------------------------

void declareMaterials ()
{
  LinearIsotropicMaterial::declare();
}


