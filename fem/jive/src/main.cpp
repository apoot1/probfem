#include <jem/base/Throwable.h>

#include <jive/app/ChainModule.h>
#include <jive/app/ControlModule.h>
#include <jive/app/OutputModule.h>
#include <jive/app/ReportModule.h>
#include <jive/app/InfoModule.h>
#include <jive/app/SampleModule.h>
#include <jive/app/UserconfModule.h>
#include <jive/geom/declare.h>
#include <jive/model/declare.h>
#include <jive/femodel/declare.h>
#include <jive/fem/declare.h>
#include <jive/fem/InputModule.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/ShapeModule.h>
#include <jive/implict/declare.h>
#include <jive/app/declare.h>
#include <jive/algebra/declare.h>
#include <jive/gl/declare.h>

#include <jem/base/ObjectTraits.h>
#include <jem/util/Tokenizer.h>

#include "models.h"
#include "modules.h"
#include "materials.h"
#include "ExposedApplication.h"
#include "CtypesUtils.h"

using namespace jem;

using jive::app::Module;
using jive::app::ChainModule;
using jive::app::OutputModule;
using jive::app::InfoModule;
using jive::app::ControlModule;
using jive::app::ReportModule;
using jive::app::SampleModule;
using jive::app::UserconfModule;
using jive::fem::InputModule;
using jive::fem::InitModule;
using jive::fem::ShapeModule;

using jem::util::Tokenizer;

//-----------------------------------------------------------------------
//   mainModule
//-----------------------------------------------------------------------


Ref<Module> mainModule ()
{
  Ref<ChainModule>    chain = newInstance<ChainModule> ();

  // Declare internal shapes, models and matrix builders. These
  // functions essentially store pointers to construction functions
  // that are called when Jive needs to create a shape, model or
  // matrix builder of a particular type.

  jive::geom:: declareIShapes     ();
  jive::geom::declareShapes       ();
  jive::model::declareModels      ();
  jive::fem::declareMBuilders     ();
  jive::implict::declareModels    ();
  jive::algebra::declareMBuilders ();
  jive::femodel::declareModels    ();

  declareModels                   ();

  // Declare all modules that you want to add dynamically.

  jive::app     ::declareModules  ();
  jive::implict ::declareModules  ();
  jive::gl      ::declareModules  ();

  declareModules                  ();

  // Declare all materials that you want to add dynamically.

  declareMaterials                ();

  // Set up the module chain. These modules will be called by Jive in
  // the order that they have been added to the chain.

  // User defined inputmodules

  chain->pushBack ( newInstance<UserconfModule> ( "userinput" ) );

  // The ShapeModule creates a ShapeTable that maps elements to shape
  // objects. This is needed, among others, by the FemViewModule
  // below.

  chain->pushBack ( newInstance<ShapeModule>() );

  // The InitModule creates the main model and initializes some other
  // stuff.

  chain->pushBack ( newInstance<InitModule>() );

  // The InfoModule prints information about the current calculation.

  chain->pushBack ( newInstance<InfoModule>() );

  // UserModules to specify solver and output

  chain->pushBack ( newInstance<UserconfModule> ( "usermodules" ) );

  // The SampleModule is used to generate simple numeric ouput

  chain->pushBack ( newInstance<SampleModule> ( ) );

  chain->pushBack ( newInstance<ControlModule> ( "control" ) );

  // Finally, the chain is wrapped in a ReportModule that prints some
  // overall information about the current calculation.

  return newInstance<ReportModule> ( "report", chain );
}


//-----------------------------------------------------------------------
//   main
//-----------------------------------------------------------------------


#ifdef __cplusplus
extern "C" {
#endif

void runFromFile ( GLOBDAT& iodat, char* fname, long flags )
{
  int argc = 2;
  char *argv[] = { (char*)"name", fname, NULL };

  Properties globdat ( "globdat" );

  globdat.set("input.nodeSet", ObjectTraits<POINTSET_PTR>::toObject(iodat.nodeSet));
  globdat.set("input.elementSet", ObjectTraits<GROUPSET_PTR>::toObject(iodat.elementSet));

  ExposedApplication::exec ( argc, argv, & mainModule, globdat );

  globdatToCtypes( iodat, globdat, flags );
}

void runFromProps ( GLOBDAT& iodat, char* strProps, long flags )
{
  Ref<Tokenizer>  tokenizer = newInstance<Tokenizer> ( strProps );

  Properties props;
  props.set ( Module::CASE_NAME, "tmp" );
  props.parseFrom ( *tokenizer, "tmp", Properties::PARSE_INCLUDE );

  Properties globdat ( "globdat" );

  globdat.set("input.nodeSet", ObjectTraits<POINTSET_PTR>::toObject(iodat.nodeSet));
  globdat.set("input.elementSet", ObjectTraits<GROUPSET_PTR>::toObject(iodat.elementSet));

  ExposedApplication::execProps ( props, & mainModule, globdat );

  globdatToCtypes( iodat, globdat, flags );
}

int run ( char* fname )
{
  int argc = 2;
  char *argv[] = { (char*)"name", fname, NULL };

  Properties globdat ( "globdat" );

  return ExposedApplication::exec ( argc, argv, & mainModule, globdat );
}

int exec ( int argc, char** argv )
{
  Properties globdat ( "globdat" );

  return ExposedApplication::exec ( argc, argv, & mainModule, globdat );
}

#ifdef __cplusplus
}
#endif
