#ifndef MODULES_H
#define MODULES_H


//-----------------------------------------------------------------------
//   public functions
//-----------------------------------------------------------------------


void  declareModules            ();
void  declareGlobdatInputModule ();
void  declareGmshInputModule    ();
void  declareGroupInputModule   ();
void  declareInputModule        ();


#endif


