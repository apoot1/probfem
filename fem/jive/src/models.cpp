
#include "models.h"


//-----------------------------------------------------------------------
//   declareModels
//-----------------------------------------------------------------------


void declareModels ()
{
  declareDirichletModel();
  declareElasticModel();
//  declareLoadDispModel();
  declareNeumannModel();
//  declareSolidModel();
  declareSpringModel();
}


