import numpy as np
import pandas as pd
from copy import deepcopy
from datetime import datetime

from probability.sampling import MCMCRunner
from experiments.inverse.three_point_hole.props import (
    get_rwm_fem_target,
    get_rwm_bfem_target,
    get_rwm_rmfem_target,
)


def linear_tempering(i):
    if i > n_burn:
        return 1.0
    else:
        return i / n_burn


n_burn = 10000
n_sample = 20000
tempering = linear_tempering

std_corruption_range = [1e-5]
h_range = [0.2, 0.1, 0.05]
h_meas = 0.2

write_output = True

for fem_type in ["fem", "bfem", "rmfem"]:

    if write_output:
        fname = "samples-{}.csv".format(fem_type)
        file = open(fname, "w")

        current_time = datetime.now().strftime("%Y/%d/%m, %H:%M:%S")
        file.write("author = Anne Poot\n")
        file.write(f"date, time = {current_time}\n")
        file.write(f"n_burn = {n_burn}\n")
        file.write(f"n_sample = {n_sample}\n")
        file.write(f"tempering = {tempering}\n")
        file.write(f"h = {h_range}\n")
        file.write(f"h_meas = fixed at {h_meas}\n")
        file.write(f"std_corruption = {std_corruption_range}\n")

    if fem_type == "fem":
        sigma_e_range = std_corruption_range
        recompute_logpdf = False

        if write_output:
            file.write(f"sigma_e = {sigma_e_range}\n")

    elif fem_type == "bfem":
        sigma_e_range = std_corruption_range
        recompute_logpdf = False

        if write_output:
            file.write(f"sigma_e = {sigma_e_range}\n")

    elif fem_type == "rmfem":
        sigma_e_range = std_corruption_range
        n_pseudomarginal = 10
        recompute_logpdf = True

        if write_output:
            file.write(f"sigma_e = {sigma_e_range}\n")
            file.write(f"n_pseudomarginal = {n_pseudomarginal}\n")

    if write_output:
        file.close()

    for std_corruption, sigma_e in zip(std_corruption_range, sigma_e_range):
        for i, h in enumerate(h_range):
            if fem_type == "fem":
                target = get_rwm_fem_target(
                    h=h,
                    h_meas=h_meas,
                    std_corruption=std_corruption,
                    sigma_e=sigma_e,
                )
            elif fem_type == "bfem":
                target = get_rwm_bfem_target(
                    h=h,
                    h_meas=h_meas,
                    std_corruption=std_corruption,
                    scale="mle",  # f_c.T @ u_c / n_c
                    sigma_e=sigma_e,
                )
            elif fem_type == "rmfem":
                target = get_rwm_rmfem_target(
                    h=h,
                    h_meas=h_meas,
                    std_corruption=std_corruption,
                    sigma_e=sigma_e,
                    n_pseudomarginal=n_pseudomarginal,
                )
            else:
                raise ValueError

            proposal = deepcopy(target.prior)
            for dist in proposal.distributions:
                dist.update_width(0.1 * dist.calc_width())
            start_value = np.array([1.0, 0.4, 0.4, np.pi / 6, 0.25])
            mcmc = MCMCRunner(
                target=target,
                proposal=proposal,
                n_sample=n_sample,
                n_burn=n_burn,
                start_value=start_value,
                seed=0,
                tempering=tempering,
                recompute_logpdf=recompute_logpdf,
                return_info=True,
            )

            samples, info = mcmc()

            if write_output:
                df = pd.DataFrame(samples, columns=["x", "y", "a", "theta", "r_rel"])

                for header, data in info.items():
                    df[header] = data

                df["sample"] = df.index
                df["h"] = h
                df["r"] = df["r_rel"] * df["a"]
                df["std_corruption"] = std_corruption

                if fem_type == "fem":
                    df["sigma_e"] = sigma_e
                elif fem_type == "bfem":
                    df["sigma_e"] = sigma_e
                elif fem_type == "rmfem":
                    df["sigma_e"] = sigma_e
                    df["n_pseudomarginal"] = n_pseudomarginal
                else:
                    raise ValueError

                write_header = (h == h_range[0]) and (sigma_e == sigma_e_range[0])
                df.to_csv(fname, mode="a", header=write_header, index=False)
