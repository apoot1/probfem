# RMFEM
Code to reproduce the Random Mesh FEM proposed by Abdulle and Garegnane [here](https://doi.org/10.1016/j.cma.2021.113961). In the folder `rmfem/reproduction/`, code can be found to reproduce several of their figures.

## Getting started
The RMFEM code relies on FEM code provided by [MyJive](https://gitlab.tudelft.nl/apoot1/myjive). For those using conda, all dependencies can be handled by simply running:

```
conda env create -f ENVIRONMENT.yml
conda activate rmfem
```
